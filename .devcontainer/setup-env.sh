#!/usr/bin/env bash

curl -fsSL https://get.pnpm.io/install.sh | sh -
echo "RUN THIS" "source /home/vscode/.bashrc"

echo "Then run this" "pnpm env use --global 18.4.0"