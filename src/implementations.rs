use an_operation_support::Bytes;
use multihash::MultihashGeneric;
#[cfg(feature = "std")]
use serde::{Deserialize, Serialize};
#[cfg(feature = "js")]
use wasm_bindgen::prelude::*;

/// A serializable wrapper for a multihash that uses unsigned integer of 64 bits.
/// Not only it must be encoded and decoded by bincode to be passed as input to other Operations,
/// but it must also be serializable by serde to be returned as last Segment result in a Workflow
#[cfg(feature = "anagolay")]
#[cfg_attr(feature = "js", wasm_bindgen)]
#[cfg_attr(feature = "std", derive(Deserialize, Serialize))]
#[derive(Debug, Clone)]
pub struct U64MultihashWrapper {
    /// The multihash code
    code: u64,
    /// The multihash digest
    digest: Bytes,
}

impl Default for U64MultihashWrapper {
    fn default() -> Self {
        U64MultihashWrapper::wrap(MultihashGeneric::default())
    }
}

impl U64MultihashWrapper {
    /// # Return
    /// The multihash code
    pub fn get_code(&self) -> u64 {
        self.code
    }

    /// # Return
    /// The multihash digest
    pub fn get_digest(&self) -> &Bytes {
        &self.digest
    }

    /// Wrap a generic multihash into [`U64MultihashWrapper`] so that it can be
    /// used as return type, which needs to be serializable
    ///
    /// # Arguments
    /// - multihash: a generic multihash that uses 64 bit unsigned integers
    ///
    /// # Return
    /// A wrapped multihash
    pub fn wrap(multihash: MultihashGeneric<64>) -> Self {
        U64MultihashWrapper {
            code: multihash.code(),
            digest: multihash.digest().to_vec(),
        }
    }
}
